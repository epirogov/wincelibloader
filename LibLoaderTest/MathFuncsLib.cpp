#include "MathFuncsLib.h"
#include <stdexcept>

double Add(double a, double b)
{
	return a + b;
}

double Subtract(double a, double b)
{
	return a - b;
}

double Multiply(double a, double b)
{
	return a * b;
}

double Divide(double a, double b)
{
	return a / b;
}

int hi()
{
	return 48;
}