//An example from https://msdn.microsoft.com/ru-ru/library/ms235627.aspx

// Returns a + b
double Add(double a, double b);

// Returns a - b
double Subtract(double a, double b);

// Returns a * b
double Multiply(double a, double b);

// Returns a / b
double Divide(double a, double b);

int hi();