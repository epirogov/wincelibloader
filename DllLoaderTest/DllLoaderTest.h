// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the DLLLOADERTEST_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// DLLLOADERTEST_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef DLLLOADERTEST_EXPORTS
#define DLLLOADERTEST_API extern "C" __declspec(dllexport)
#else
#define DLLLOADERTEST_API __declspec(dllimport)
#endif

// This class is exported from the DllLoaderTest.dll
class CDllLoaderTest {
public:
	CDllLoaderTest(void);
	// TODO: add your methods here.
};

//extern DLLLOADERTEST_API int nDllLoaderTest;

DLLLOADERTEST_API int fnDllLoaderTest(void);


// Returns a + b
DLLLOADERTEST_API double MyAdd(double a, double b);

// Returns a - b
DLLLOADERTEST_API double MySubtract(double a, double b);

// Returns a * b
DLLLOADERTEST_API double MyMultiply(double a, double b);

// Returns a / b
DLLLOADERTEST_API double MyDivide(double a, double b);

DLLLOADERTEST_API int MyHi();