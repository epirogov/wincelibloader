// DllLoaderTest.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "DllLoaderTest.h"
#include "MathFuncsLib.h"


BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
    return TRUE;
}

// This is an example of an exported variable
DLLLOADERTEST_API int nDllLoaderTest=0;

// This is an example of an exported function.
DLLLOADERTEST_API int fnDllLoaderTest(void)
{
	return 42;
}

// Returns a + b
DLLLOADERTEST_API double MyAdd(double a, double b)
{
	return Add(a, b);
}

// Returns a - b
DLLLOADERTEST_API double MySubtract(double a, double b)
{
	return Subtract(a, b);
}

// Returns a * b
DLLLOADERTEST_API double MyMultiply(double a, double b)
{
	return Multiply(a, b);
}

// Returns a / b
DLLLOADERTEST_API double MyDivide(double a, double b)
{
	return Divide(a, b);
}

DLLLOADERTEST_API int MyHi()
{
	return hi();
}


// This is the constructor of a class that has been exported.
// see DllLoaderTest.h for the class definition
CDllLoaderTest::CDllLoaderTest()
{ 
	return; 
}
