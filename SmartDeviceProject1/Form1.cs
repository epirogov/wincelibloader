﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace SmartDeviceProject1
{
    public partial class Form1 : Form
    {
        [DllImport("DllLoaderTest.dll", CharSet = CharSet.Unicode)]
        public static extern double MyAdd(double a, double b);
        [DllImport("DllLoaderTest.dll", CharSet = CharSet.Unicode)]
        public static extern int fnDllLoaderTest();
        [DllImport("DllLoaderTest.dll", CharSet = CharSet.Unicode)]
        public static extern int MyHi();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
  //          System.MissingMethodException was unhandled
  //Message="Can't find PInvoke DLL 'DllLoaderTest.dll'."
  //StackTrace:
  //     at SmartDeviceProject1.Form1.Form1_Load(Object sender, EventArgs e)
  //     at System.Windows.Forms.Form.OnLoad(EventArgs e)
  //     at System.Windows.Forms.Form._SetVisibleNotify(Boolean fVis)
  //     at System.Windows.Forms.Control.set_Visible(Boolean value)
  //     at System.Windows.Forms.Application.Run(Form fm)
  //     at SmartDeviceProject1.Program.Main()

  //          System.NotSupportedException was unhandled
  //Message="NotSupportedException"
  //StackTrace:
  //     at SmartDeviceProject1.Form1.Form1_Load(Object sender, EventArgs e)
  //     at System.Windows.Forms.Form.OnLoad(EventArgs e)
  //     at System.Windows.Forms.Form._SetVisibleNotify(Boolean fVis)
  //     at System.Windows.Forms.Control.set_Visible(Boolean value)
  //     at System.Windows.Forms.Application.Run(Form fm)
  //     at SmartDeviceProject1.Program.Main()


            //https://social.msdn.microsoft.com/Forums/en-US/fb43e94f-4f43-4435-a01d-6ca42fb605f0/notsupportedexception-in-pinvoke-for-netcf-c-project?forum=vssmartdevicesvbcs

            this.label1.Text = MyHi().ToString();
            //this.label1.Text = fnDllLoaderTest().ToString();
        }
    }
}